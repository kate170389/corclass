package com.makhova.repositories;

import com.makhova.common.Source;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = LoaderApplication.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@SqlGroup({
        @Sql(executionPhase = BEFORE_TEST_METHOD, scripts = {"classpath:data_model.sql"}),
        @Sql(executionPhase = AFTER_TEST_METHOD, scripts = "classpath:remove.sql")
})
public class SourceRepositoryTest {
    @Autowired
    private SourceRepository sourceRepository;

    @Test
    public void saveAll() {
        Source one = new Source(1, "AAA", new BigDecimal(1.1));
        Source two = new Source(2, "BBB", new BigDecimal(1.1));
        List<Source> sources = Arrays.asList(one, two);
        assertEquals(sources, sourceRepository.saveAll(sources));
        long expectedSize = 2;
        assertEquals(expectedSize, sourceRepository.count());
    }
}