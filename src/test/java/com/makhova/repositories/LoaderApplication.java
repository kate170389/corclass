package com.makhova.repositories;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableTransactionManagement(mode= AdviceMode.ASPECTJ)
public class LoaderApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(com.makhova.LoaderApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
    }
}
