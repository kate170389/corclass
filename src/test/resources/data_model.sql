CREATE TABLE IF NOT EXISTS source (
  id               INT(11)          NOT NULL,
  name        VARCHAR(45) NOT NULL,
  value DECIMAL(3,2) NOT NULL,
  PRIMARY KEY (id)
  );