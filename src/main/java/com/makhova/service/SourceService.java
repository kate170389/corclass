package com.makhova.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.makhova.common.Source;
import com.makhova.exceptions.SourceServiceException;
import com.makhova.repositories.SourceRepository;
import com.makhova.utils.TransactionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SourceService {
    private static final Logger logger = LoggerFactory.getLogger(SourceService.class);
    @Value("${csv.max.values}")
    private int maxValues;
    @Value("${directory.from}")
    private String pathFrom;
    @Value("${directory.to}")
    private String pathTo;
    private SourceRepository sourceRepository;
    private File directoryFrom;
    private File directoryTo;
    private TransactionHandler transactionHandler;

    @Autowired
    public SourceService(SourceRepository sourceRepository, TransactionHandler transactionHandler) {
        this.sourceRepository = sourceRepository;
        this.transactionHandler = transactionHandler;
    }

    @PostConstruct
    public void init() {
        logger.debug("Init directories={}, {}.", pathFrom, pathTo);
        directoryFrom = new File(pathFrom).getAbsoluteFile();
        directoryTo = new File(pathTo).getAbsoluteFile();
    }

    private void checkDirectories() {
        logger.debug("Check directories={}, {}.", pathFrom, pathTo);
        if (!(directoryFrom.exists() && directoryTo.exists()
                && directoryFrom.isDirectory() && directoryTo.isDirectory())) {
            throw new SourceServiceException("Wrong directories!");
        }
    }

    private Path moveFile(File csvFile) {
        logger.debug("MoveFile from{} to{}.", Paths.get(csvFile.getPath()),
                Paths.get(pathTo + "/" + csvFile.getName()));
        try {
            return Files.move(Paths.get(csvFile.getPath()), Paths.get(pathTo + "/" + csvFile.getName()));
        } catch (IOException e) {
            logger.error("Error:", e);
            throw new SourceServiceException(e);
        }
    }

    public void saveAndMoveFiles() {
        logger.debug("Start method {}.saveAndMoveFiles", this.getClass());
        checkDirectories();
        File[] arrayFiles;
        if ((arrayFiles = directoryFrom.listFiles()) != null) {
            Arrays.stream(arrayFiles).filter(file -> !file.isDirectory() && file.getName().endsWith(".csv"))
                    .collect(Collectors.toList())
                    .forEach(file -> {
                        transactionHandler.runInTransaction(() -> processingCSV(file));
                    });
        }
    }

    private long processingCSV(File csvFile) {
        logger.debug("Start method {}.getFromCSV", this.getClass());
        List<Source> sources = new ArrayList<>();
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            MappingIterator<Source> readValues =
                    mapper.readerFor(Source.class).with(bootstrapSchema).readValues(csvFile);
            while (readValues.hasNextValue()) {
                sources.add(readValues.nextValue());
                if (sources.size() == maxValues || !readValues.hasNextValue()) {
                    sourceRepository.saveAll(sources);
                    sources.clear();
                }
            }
            transactionHandler.runInTransaction(() -> moveFile(csvFile));
            return csvFile.length();
        } catch (IOException e) {
            logger.error("Error:", e);
            throw new SourceServiceException(e);
        }
    }
}