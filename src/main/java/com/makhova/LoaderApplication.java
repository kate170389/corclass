package com.makhova;

import com.makhova.service.SourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement(mode= AdviceMode.ASPECTJ)
public class LoaderApplication implements ApplicationRunner {
    private SourceService sourceService;
    private final ConfigurableApplicationContext context;

    @Autowired
    public LoaderApplication(SourceService sourceService, ConfigurableApplicationContext context) {
        this.sourceService = sourceService;
        this.context = context;
    }

    public static void main(String[] args) {
        SpringApplication.run(LoaderApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        System.out.println("Application start");
        sourceService.saveAndMoveFiles();
        SpringApplication.exit(context);
    }
}