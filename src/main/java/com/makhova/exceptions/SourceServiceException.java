package com.makhova.exceptions;

public class SourceServiceException extends RuntimeException {
    public SourceServiceException(String msg) {
        super(msg);
    }

    public SourceServiceException(Throwable couse) {
        super(couse);
    }
}