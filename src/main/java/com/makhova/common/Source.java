package com.makhova.common;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "source")
@Data
public class Source {
    @Id
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, precision = 3, scale = 2)
    private BigDecimal value;

    public Source(int id, String name, BigDecimal value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }
}